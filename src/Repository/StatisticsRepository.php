<?php

namespace Repository;

use PDO;
use PDOException;

class StatisticsRepository
{

    public function getMostUsedRequestParameters()
    {
        $pdo = Db::connect();

        $sql = "
SELECT
    `int1`, 
    `int2`, 
    `str1`, 
    `str2`, 
    `limit`, 
    `counter`
FROM `fizzbuzzRequestParameters`
ORDER BY counter DESC
LIMIT 1";

        try {

            $query = $pdo->prepare($sql);
            $query->execute();
            $requestParameters = $query->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

        Db::disconnect();
        return $requestParameters;
    }

    public function insertOrUpdateRequestParameters($int1, $int2, $str1, $str2, $limit)
    {
        $pdo = Db::connect();

        $data = array(
            'int1' => $int1,
            'int2' => $int2,
            'str1' => $str1,
            'str2' => $str2,
            'limit' => $limit,
        );
        $sql = "INSERT INTO fizzbuzzRequestParameters (`int1`, `int2`, `str1`, `str2`, `limit`, `counter`) VALUES (:int1, :int2, :str1, :str2, :limit, 1) ON DUPLICATE KEY UPDATE counter = counter + 1";

        try {
            $query = $pdo->prepare($sql);
            $query->execute($data);

        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

        Db::disconnect();
    }
}