<?php

namespace Service;

use Parameters\ParametersEntity;
use Repository\StatisticsRepository;

class StatisticsService
{

    /**
     * @var StatisticsRepository
     */
    private $_statisticsRepository;

    public function __construct(StatisticsRepository $_statisticsRepository)
    {
        $this->_statisticsRepository = $_statisticsRepository;
    }


    public function getMostUsedRequestParametersOutput() {
        $requestParameters = $this->_statisticsRepository->getMostUsedRequestParameters();
        return $this->_formatRequestParameters($requestParameters);
    }

    /**
     * Format the output of the statistics endpoint
     * @param array $requestParameters
     * @return string
     */
    protected function _formatRequestParameters(array $requestParameters)
    {
        $output = "Most used request had the following parameters : ";
        $output .= $this->getSingleParameterOutput($requestParameters, ParametersEntity::INT1) . " & ";
        $output .= $this->getSingleParameterOutput($requestParameters, ParametersEntity::INT2) . " & ";
        $output .= $this->getSingleParameterOutput($requestParameters, ParametersEntity::STR1) . " & ";
        $output .= $this->getSingleParameterOutput($requestParameters, ParametersEntity::STR2) . " & ";
        $output .= $this->getSingleParameterOutput($requestParameters, ParametersEntity::LIMIT) . ". ";
        $output .= "It has been used " . $requestParameters[ParametersEntity::COUNTER] ." time(s).";

        return $output;
    }

    /**
     * @param array $requestParameters
     * @param string $parameter
     * @return string
     */
    protected function getSingleParameterOutput(array $requestParameters, $parameter): string
    {
        return  $parameter . "=" . $requestParameters[$parameter];
    }
}