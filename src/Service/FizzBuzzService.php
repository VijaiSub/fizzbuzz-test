<?php

namespace Service;

use Parameters\ParametersEntity;
use Repository\StatisticsRepository;

/**
 * Class FizzBuzzService
 * @package Service
 */
class FizzBuzzService {

    /**
     * @var int
     */
    private $_int1;

    /**
     * @var int
     */
    private $_int2;

    /**
     * @var string
     */
    private $_str1;

    /**
     * @var string
     */
    private $_str2;

    /**
     * @var int
     */
    private $_limit;

    /**
     * @var StatisticsRepository
     */
    private $_statisticsRepository;

    public function __construct(ParametersEntity $parametersEntity, StatisticsRepository $statisticsRepository)
    {
        $this->_int1 = $parametersEntity->getInt1();
        $this->_int2 = $parametersEntity->getInt2();
        $this->_str1 = $parametersEntity->getStr1();
        $this->_str2 = $parametersEntity->getStr2();
        $this->_limit = $parametersEntity->getLimit();
        $this->_statisticsRepository = $statisticsRepository;
    }

    /**
     * @return array
     */
    function computeFizzBuzzString()
    {
        $this->_statisticsRepository->insertOrUpdateRequestParameters($this->_int1, $this->_int2, $this->_str1, $this->_str2, $this->_limit);
        $fizzBuzzStr = "";
        for ($i = 1; $i <= $this->_limit; $i++)
        {
            if ($i > 1) {
                $fizzBuzzStr .= " ";
            }
            //First, the most frequent case for $i value
            if ($i % $this->_int1 != 0  && $i % $this->_int2 != 0)
            {
                $fizzBuzzStr .= $i;
                continue;
            }
            if ($i % $this->_int1 == 0)
            {
                $fizzBuzzStr .= $this->_str1;
            }

            if ($i % $this->_int2 == 0)
            {
                $fizzBuzzStr .= $this->_str2;
            }
        }

        return array($fizzBuzzStr);
    }
}