<?php

namespace Parameters;

use Parameters\Validators\DefaultValidator;
use Parameters\Validators\IntValidator;
use Parameters\Validators\StringValidator;
use Parameters\Validators\ValidatorInterface;

class ParametersEntity {

    const INT1  = "int1";
    const INT2  = "int2";
    const STR1  = "str1";
    const STR2  = "str2";
    const LIMIT = "limit";
    const COUNTER = "counter";

    private $_int1;
    private $_int2;
    private $_str1;
    private $_str2;
    private $_limit;

    /**
     * @return int
     */
    public function getInt1() : int
    {
        return $this->_int1;
    }

    /**
     * @return int
     */
    public function getInt2() : int
    {
        return $this->_int2;
    }

    /**
     * @return string
     */
    public function getStr1() : string
    {
        return $this->_str1;
    }

    /**
     * @return string
     */
    public function getStr2() : string
    {
        return $this->_str2;
    }

    /**
     * @return int
     */
    public function getLimit() : int
    {
        return $this->_limit;
    }

    /**
     * @param $parameters
     */
    public function setParameters($parameters)
    {
        $this->_int1 = $parameters[self::INT1];
        $this->_int2 = $parameters[self::INT2];
        $this->_str1 = $parameters[self::STR1];
        $this->_str2 = $parameters[self::STR2];
        $this->_limit = $parameters[self::LIMIT];
    }

    /**
     * @param $parameters
     * @return bool
     */
    public function validateParameters($parameters): bool
    {
        foreach ($this->_getMandatoryParameters() as $mandatoryParameter) {
            //check if mandatory parameter exists
            if (isset($parameters[$mandatoryParameter]) === false) {
                return false;
            }

            //check if mandatory parameter has valid format
            $validator = $this->_createParameterValidator($mandatoryParameter);
            if ($validator->check($parameters[$mandatoryParameter]) === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     */
    protected function _getMandatoryParameters() {
        return array(
            self::INT1,
            self::INT2,
            self::STR1,
            self::STR2,
            self::LIMIT
        );
    }

    /**
     * @param string $parameter
     * @return ValidatorInterface
     */
    protected function _createParameterValidator(string $parameter) {
        switch ($parameter) {
            case self::INT1:
            case self::INT2:
            case self::LIMIT:
                $validator = new IntValidator();
                break;
            case self::STR1:
            case self::STR2:
                $validator = new StringValidator();
                break;
            default:
                $validator = new DefaultValidator();
        }

        return $validator;
    }
}