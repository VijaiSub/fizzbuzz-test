<?php

namespace Parameters\Validators;

/**
 * Interface ValidatorInterface
 */
interface ValidatorInterface
{
    /**
     * @param mixed $value
     *
     * @return boolean
     */
    public function check($value);

}