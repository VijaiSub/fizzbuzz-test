<?php

namespace Parameters\Validators;

class StringValidator implements ValidatorInterface
{

    /**
     * @inheritDoc
     */
    public function check($value)
    {
        return is_string($value);
    }
}