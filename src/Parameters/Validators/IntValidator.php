<?php

namespace Parameters\Validators;

class IntValidator implements ValidatorInterface
{

    /**
     * @inheritDoc
     */
    public function check($value)
    {
        return (filter_var($value, FILTER_VALIDATE_INT)) && ((int)$value != 0) ;
    }
}