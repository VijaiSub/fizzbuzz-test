<?php

namespace Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Parameters\ParametersEntity;
use Repository\StatisticsRepository;

require __DIR__ . '/../../../src/Service/FizzBuzzService.php';

class FizzBuzzServiceTest extends TestCase
{

    public function testComputeFizzBuzzReturnsCorrectArrayWhenInt1Is3AndInt2Is5() {

        $parametersEntityDouble = $this->_createParametersEntityDouble(3, 5, "Fizz", "Buzz", 15);
        $statisticRepositoryDouble = $this->_createStatisticsRepositoryDouble();
        $fizzBuzzService = new FizzBuzzService($parametersEntityDouble, $statisticRepositoryDouble);

        $fizzBuzzResult = $fizzBuzzService->computeFizzBuzzString();

        $expectedFizzBuzz = array("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz");
        $this->assertEquals($expectedFizzBuzz, $fizzBuzzResult);
    }

    public function testComputeFizzBuzzReturnsCorrectArrayWhenLimitIsSmallerThanInt1AndInt2() {

        $parametersEntityDouble = $this->_createParametersEntityDouble(11, 12, "Fizz", "Buzz", 10);
        $statisticRepositoryDouble = $this->_createStatisticsRepositoryDouble();
        $fizzBuzzService = new FizzBuzzService($parametersEntityDouble, $statisticRepositoryDouble);

        $fizzBuzzResult = $fizzBuzzService->computeFizzBuzzString();

        $expectedFizzBuzz = array("1 2 3 4 5 6 7 8 9 10");
        $this->assertEquals($expectedFizzBuzz, $fizzBuzzResult);
    }

    /**
     * @param int $int1
     * @param int $int2
     * @param string $str1
     * @param string $str2
     * @param int $limit
     * @return MockObject | ParametersEntity
     */
    protected function _createParametersEntityDouble($int1, $int2, $str1, $str2, $limit): MockObject
    {
        $methods = [
            'getInt1',
            'getInt2',
            'getStr1',
            'getStr2',
            'getLimit'
        ];
        $parametersEntityDouble =$this->getMockBuilder(ParametersEntity::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();

        $parametersEntityDouble->method('getInt1')
            ->willReturn($int1);

        $parametersEntityDouble->method('getInt2')
            ->willReturn($int2);

        $parametersEntityDouble->method('getStr1')
            ->willReturn($str1);

        $parametersEntityDouble->method('getStr2')
            ->willReturn($str2);

        $parametersEntityDouble->method('getLimit')
            ->willReturn($limit);

        return $parametersEntityDouble;
    }

    /**
     * @return MockObject | StatisticsRepository
     */
    protected function _createStatisticsRepositoryDouble(): MockObject
    {
        $methods = [
            'insertOrUpdateRequestParameters'
        ];

        $statisticsRepositoryDouble =  $this->getMockBuilder(StatisticsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();

        $statisticsRepositoryDouble->method('insertOrUpdateRequestParameters')
            ->willReturn(true);

        return $statisticsRepositoryDouble;
    }

}