<?php

use Parameters\Validators\DefaultValidator;
use PHPUnit\Framework\TestCase;

class DefaultValidatorTest extends TestCase
{

    public function testCheck()
    {
        $validator = new DefaultValidator();

        $result = $validator->check("random value");
        
        $this->assertTrue($result);
    }
}
