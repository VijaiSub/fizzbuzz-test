<?php

namespace Parameters\Validators;

use PHPUnit\Framework\TestCase;

class StringValidatorTest extends TestCase
{

    public function testCheckWithStringReturnsTrue()
    {
        $validator = new StringValidator();

        $result = $validator->check("string");
        
        $this->assertTrue($result);
    }

    public function testCheckWithNumberReturnsFalse()
    {
        $validator = new StringValidator();

        $result = $validator->check(42);
        
        $this->assertFalse($result);
    }

}
