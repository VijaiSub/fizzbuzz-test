<?php

namespace Parameters\Validators;


use PHPUnit\Framework\TestCase;

class IntValidatorTest extends TestCase
{

    public function testCheckWithIntReturnsTrue()
    {
        $validator = new IntValidator();

        $result = $validator->check(42);
        
        $this->assertTrue($result);
    }

    public function testCheckWithIntStringReturnsTrue()
    {
        $validator = new IntValidator();

        $result = $validator->check("42");
        
        $this->assertTrue($result);
    }

    public function testCheckWithFloatReturnsFalse()
    {
        $validator = new IntValidator();

        $result = $validator->check(4.2);
        
        $this->assertFalse($result);
    }

    public function testCheckWithNonNumericStringReturnsFalse()
    {
        $validator = new IntValidator();

        $result = $validator->check("abc");
        
        $this->assertFalse($result);
    }
}
