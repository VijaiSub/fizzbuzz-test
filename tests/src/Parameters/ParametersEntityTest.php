<?php

namespace Parameters;

use Parameters\Validators\ValidatorInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ParametersEntityTest extends TestCase
{

    public function testValidateParametersReturnsTrueWhenParametersAreValid() {
        $params = array(
            "int1" => "3",
            "int2" => "5",
            "str1" => "Fizz",
            "str2" => "Buzz",
            "limit" => "15"
        );
        $parametersEntity = $this->_createParametersEntityDouble();

        $result = $parametersEntity->validateParameters($params);

        $this->assertTrue($result);
    }

    public function testValidateParametersReturnsFalseWhenOneMandatoryParameterIsMissing() {
        $params = array(
            "int2" => "5",
            "str1" => "Fizz",
            "str2" => "Buzz",
            "limit" => "15"
        );
        $parametersEntity = $this->_createParametersEntityDouble();

        $result = $parametersEntity->validateParameters($params);

        $this->assertFalse($result);
    }

    /**
     * @return MockObject | ParametersEntity
     */
    protected function _createParametersEntityDouble()
    {
        $validatorDouble = $this->_createValidatorDouble();
        $methods = [
            '_createParameterValidator'
        ];
        $parametersEntityDouble =$this->getMockBuilder(ParametersEntity::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();

        $parametersEntityDouble->method('_createParameterValidator')
            ->willReturn($validatorDouble);

        return $parametersEntityDouble;
    }

    protected function _createValidatorDouble()
    {
        $methods = [
            'check'
        ];
        $validatorDouble = $this->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();

        $validatorDouble->method('check')
            ->willReturn(true);

        return $validatorDouble;
    }
}
