# fizzbuzz-test

This is an implementation of Fizz Buzz test. This service provides a REST API that will output according to the following rules : 
- Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.
- Returns a list of strings with numbers from 1 to limit, where: all multiples of int1 are replaced by str1, all multiples of int2 are replaced by str2, all multiples of int1 and int2 are replaced by str1str2.

  

## Install

### Composer
```
composer install
```

###Mysql
Execute the mysql patch located at sql/init.sql
## API

### FizzBuzz

```
<!--Curl-->
http://<host>:<port>/fizzbuzz-test/fizzbuzz/int1=<int1>&int2=<int2>&str1=<str1>&str2=<str2>&limit=<limit>
```

| Parameter | Description
| ----- | -----------
| int1 | integer which multiples will be replaced by <str1>
| int2 | integer which multiples will be replaced by <str2>
| str1 | string that will replace all multiples of <int1>
| str2 | string that will replace all multiples of <int2>
| limit | last value of the fizzbuzz


- **200 OK**
```
HTTP/1.1
{
    "Response": [
        "1",
        "2",
        "fizz",
        "4",
        "buzz",
        "fizz",
        "7",
        "8",
        "fizz",
        "buzz",
        "11",
        "fizz",
        "13",
        "14",
        "fizzbuzz"
    ]
}
```

- **400 Bad request**
```
HTTP/1.1 400
{
    "message": "Sorry, Bad Request !",
    "status": " Bad Request"
}
```

### Statistics

```
<!--Curl-->
http://<host>:<port>/fizzbuzz-test/statistics
```

- **200 OK**
```
HTTP/1.1
{
    "Response": "Most used request had the following parameters : int1=3 & int2=5 & str1=fizz & str2=buzz & limit=100. It has been used 10 time(s)."
}
```