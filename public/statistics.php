﻿<?php

use Repository\StatisticsRepository;
use Service\StatisticsService;

require_once './bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET)) {
    $statisticsRepository = new StatisticsRepository();
    $statisticsService  = new StatisticsService($statisticsRepository);
    $statisticsOutput = $statisticsService->getMostUsedRequestParametersOutput();
    if(empty($statisticsOutput))
        header("HTTP/1.1 404 Not Found");
    echo json_encode($statisticsOutput);
} else {
    echo "Sorry, Bad request !";
    header("HTTP/1.1 400 Bad Request");
}