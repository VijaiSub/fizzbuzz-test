﻿<?php

use Parameters\ParametersEntity;
use Repository\StatisticsRepository;
use Service\FizzBuzzService;

require_once './bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET)) {
    $parametersEntity = new ParametersEntity();
    if ($parametersEntity->validateParameters($_GET) === false) {
        echo "Sorry, Bad request !";
        header("HTTP/1.1 400 Bad Request");
    } else {
        $parametersEntity->setParameters($_GET);
        $statisticsRepository = new StatisticsRepository();
        $fizzBuzzService  = new FizzBuzzService($parametersEntity, $statisticsRepository);
        $fizzBuzzString = $fizzBuzzService->computeFizzBuzzString();
        if(empty($fizzBuzzString))
            header("HTTP/1.1 404 Not Found");
        echo json_encode($fizzBuzzString);
    }
} else {
    echo "Sorry, Bad request !";
    header("HTTP/1.1 400 Bad Request");
}