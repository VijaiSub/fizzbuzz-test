<?php

header('content-type: application/json');

spl_autoload_register(function($class) {
    $classPath = '../src/' . str_replace('\\', '/', $class) . '.php';
    include $classPath;
});
