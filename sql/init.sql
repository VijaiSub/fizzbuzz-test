CREATE DATABASE statistics;

USE statistics;

CREATE TABLE `fizzbuzzRequestParameters` (
  `id`      INT UNSIGNED       NOT NULL AUTO_INCREMENT,
  `int1`    INT UNSIGNED NOT NULL,
  `int2`    INT UNSIGNED NOT NULL,
  `str1`    VARCHAR(255) NOT NULL,
  `str2`    VARCHAR(255) NOT NULL,
  `limit`   INT UNSIGNED NOT NULL,
  `counter` INT UNSIGNED NOT NULL,
  UNIQUE KEY `int1_int2_str1_str2_limit` (`int1`, `int2`, `str1`, `str2`, `limit`),
  INDEX `counter` (`counter`),
  PRIMARY KEY (`id`)
);